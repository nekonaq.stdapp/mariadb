class mariadb::params {
  include stdapp

  $app_dir = "${stdapp::base_dir}/mariadb"
  $service_prefix = 'docker-compose@mariadb'

  $value = parseyaml(file('mariadb/params.yml'))
  $settings = std::nv($value['mariadb::settings'], Hash, {})
}
