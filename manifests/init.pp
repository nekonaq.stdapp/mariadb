class mariadb(
  Variant[String, Boolean] $ensure = present,
  Variant[String, Boolean] $service_ensure = running,
  Optional[Hash] $settings = {},
) inherits mariadb::params {
  notice('---')

  $app_settings = $mariadb::params::settings + std::nv($settings, Hash, {})
  info(std::pp({'app_settings' => $app_settings}))

  $file_ensure = std::file_ensure($ensure)
  $dir_ensure = std::dir_ensure($ensure)

  contain mariadb::install
  contain mariadb::config
  contain mariadb::service

  case $ensure {
    'absent', false: {
      # absent の場合は disable service を先に行う
      Class['mariadb::service']
      -> Class['mariadb::install']
      -> Class['mariadb::config']
    }
    default: {
      Class['mariadb::install']
      -> Class['mariadb::config']
      -> Class['mariadb::service']
    }
  }
}

class mariadb::install {
  #// config files
  file {
    default:
      ensure => $mariadb::dir_ensure,
      force => true,
      ;
    'mariadb::config':
      path => "${mariadb::app_dir}/config",
      require => File['mariadb::app_dir'],
      ;
    'mariadb::config::mysql':
      path => "${mariadb::app_dir}/config/mysql",
      require => File['mariadb::config'],
      ;
    'mariadb::config::mysql/conf.d':
      path => "${mariadb::app_dir}/config/mysql/conf.d",
      require => File['mariadb::config::mysql'],
      ;
    /**/
    'mariadb::secrets':
      path => "${mariadb::app_dir}/secrets",
      replace => false,
      require => File['mariadb::app_dir'],
      ;
    #*/
  }

  /**/
  $secrets_root_password = 'root-password'
  file { "mariadb::secrets/${secrets_root_password}":
    path => "${mariadb::app_dir}/secrets/${secrets_root_password}",
    ensure => $mariadb::file_ensure,
    replace => false,
    content => std::mkpw(4),
    require => File['mariadb::secrets'],
    * => $stdapp::secrets_perm,
  }
  #*/

  $conf_env = 'mysql.env'
  file { "mariadb::config::${conf_env}":
    path => "${mariadb::app_dir}/config/${conf_env}",
    ensure => $mariadb::file_ensure,
    content => epp("mariadb/config/${conf_env}", $mariadb::app_settings),
    require => File['mariadb::config'],
  }

  $conf_mysqld = 'mysql/conf.d/mysqld.cnf'
  file { 'mariadb::config::${conf_mysqld}.cnf':
    path => "${mariadb::app_dir}/config/${conf_mysqld}",
    ensure => $mariadb::file_ensure,
    content => epp("mariadb/config/${conf_mysqld}", $mariadb::app_settings),
    require => File['mariadb::config::mysql/conf.d'],
  }

  $conf_mysqldump = "mysql/conf.d/mysqldump.cnf"
  file { 'mariadb::config::${conf_mysqldump}':
    path => "${mariadb::app_dir}/config/${conf_mysqldump}",
    ensure => $mariadb::file_ensure,
    content => epp("mariadb/config/${conf_mysqldump}", $mariadb::app_settings),
    require => File['mariadb::config::mysql/conf.d'],
  }

  #// setup service
  stdapp::service::install { mariadb: }
}

class mariadb::config {
  #//empty
}

class mariadb::service {
  stdapp::service { mariadb: }
}
